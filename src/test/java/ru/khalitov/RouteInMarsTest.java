package ru.khalitov;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by almaz-h on 08.05.2021.
 */
@RunWith(Parameterized.class)
public class RouteInMarsTest {
    char[][] map;
    char[][] expected;
    RouteFinder routeFinder;

    public RouteInMarsTest(char[][] map, char[][] expected) {
        routeFinder = new RouteInMars() ;
        this.map = map;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection findPathOnMars() {
        return Arrays.asList(new Object[][] {
                {
                        new char[][] {
                                ".......@.".toCharArray(),
                                ".#######.".toCharArray(),
                                "##.......".toCharArray(),
                                "####...##".toCharArray(),
                                "##...####".toCharArray(),
                                ".......##".toCharArray(),
                                ".X...####".toCharArray(),
                        },
                        new char[][] {
                                ".......@+".toCharArray(),
                                ".#######+".toCharArray(),
                                "##....+++".toCharArray(),
                                "####+++##".toCharArray(),
                                "##..+####".toCharArray(),
                                "....+..##".toCharArray(),
                                ".X+++####".toCharArray(),
                        }
                },
                {
                        new char[][] {
                                "...@.".toCharArray(),
                                ".##.#".toCharArray(),
                                ".....".toCharArray(),
                                "#####".toCharArray(),
                                ".X...".toCharArray(),
                        },
                        null
                },
                {
                        new char[][] {
                                "...@..".toCharArray(),
                                ".####.".toCharArray(),
                                "......".toCharArray(),
                                "####..".toCharArray(),
                                ".X....".toCharArray(),
                        },
                        new char[][] {
                                "...@++".toCharArray(),
                                ".####+".toCharArray(),
                                ".....+".toCharArray(),
                                "####.+".toCharArray(),
                                ".X++++".toCharArray(),
                        }
                },
        });
    }

    @Test
    public void testFindRoute() {
        assertEquals(expected, routeFinder.findRoute(map));
    }
}