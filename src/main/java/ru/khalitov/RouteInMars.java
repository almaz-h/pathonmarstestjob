package ru.khalitov;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by almaz-h on 06.05.2021.
 */
public class RouteInMars implements RouteFinder {

    private final int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    private final char startPosition = '@';
    private final char endPosition = 'X';
    private final char wallPosition = '#';
    private final char pathPosition = '+';

    private HashSet<MapCoordinates> visitedCoordinate;
    private ArrayDeque<MapCoordinates> deque;

    public static void main(String[] args) {
        RouteInMars routeInMars = new RouteInMars();
        char[][] resultRoute = routeInMars.findRoute(new char[][]
                {"...@.".toCharArray(),
                 ".####".toCharArray(),
                 ".....".toCharArray(),
                 "####.".toCharArray(),
                 ".X...".toCharArray()});

        for (char[] chars : resultRoute) {
            for (char aChar : chars) {
                System.out.print(aChar + " ");
            }
            System.out.println();
        }
    }

    /**
     * Поиск кратчайшего маршрута между двумя точками
     *
     * @param map карта
     * @return карта с простроенным маршрутом
     */
    @Override
    public char[][] findRoute(char[][] map) {
        MapCoordinates startingPosition = findStartPosition(map, startPosition);
        deque = new ArrayDeque<>();
        visitedCoordinate = new HashSet<>(map.length * map[0].length);

        if (startingPosition == null)
            return null;
        deque.add(startingPosition);
        visitedCoordinate.add(startingPosition);
        MapCoordinates endPosition = bypassMapCoordinates(map);
        return endPosition != null ? buildPath(map, endPosition) : null;
    }

    private MapCoordinates findStartPosition(char[][] map, char symbol) {
        for (int i = 0; i < map.length; i++)
            for (int j = 0; j < map[0].length; j++)
                if (map[i][j] == symbol)
                    return new MapCoordinates(i, j, null);
        return null;
    }

    /**
     * Обойти координаты карты
     *
     * @param map карта
     * @return возвращает путь до конечной позиции
     */
    private MapCoordinates bypassMapCoordinates(char[][] map) {
        while (deque.size() > 0) {
            MapCoordinates position = deque.pop();
            ArrayList<MapCoordinates> nearPositions = nearsFind(map, position);
            for (MapCoordinates nearby : nearPositions) {
                if (!visitedCoordinate.contains(nearby)) {
                    nearby.setParent(position);
                    deque.add(nearby);
                    visitedCoordinate.add(nearby);
                }
                if (map[nearby.getX()][nearby.getY()] == endPosition)
                    return nearby;
            }
        }
        return null;
    }

    /**
     * Поиск ближайших соседей для клетки
     *
     * @param map  карта
     * @param cell клетка
     * @return соседи для position
     */
    private ArrayList<MapCoordinates> nearsFind(char[][] map, MapCoordinates cell) {
        ArrayList<MapCoordinates> result = new ArrayList<>();
        for (int[] dir : directions) {
            int x = cell.getX() + dir[0];
            int y = cell.getY() + dir[1];

            if (!isOutOfMap(x, y, map) && !isWall(x, y, map))
                result.add(new MapCoordinates(x, y, cell));
        }
        return result;
    }

    private boolean isWall(int x, int y, char[][] map) {
        return map[x][y] == wallPosition;
    }

    private boolean isOutOfMap(int x, int y, char[][] map) {
        return (x < 0 || x >= map.length || y < 0 || y >= map[0].length);
    }

    private char[][] buildPath(char[][] map, MapCoordinates endPosition) {
        MapCoordinates parent = endPosition.getParent();
        while (parent != null && parent.getParent() != null) {
            map[parent.getX()][parent.getY()] = pathPosition;
            parent = parent.getParent();
        }
        return map;
    }
}
