package ru.khalitov;

/**
 * Created by almaz-h on 06.05.2021.
 */
public interface RouteFinder {
    /**
     * Поиск кратчайшего маршрута между двумя точками
     *
     * @param map карта
     * @return карта с простроенным маршрутом
     */
    char[][] findRoute(char[][] map);
}
