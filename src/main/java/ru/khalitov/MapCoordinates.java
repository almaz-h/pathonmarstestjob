package ru.khalitov;

import java.util.Objects;

/**
 * Created by almaz-h on 06.05.2021.
 */
public class MapCoordinates {
    private int x;
    private int y;
    MapCoordinates parent;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public MapCoordinates getParent() {
        return parent;
    }

    public void setParent(MapCoordinates parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapCoordinates that = (MapCoordinates) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public MapCoordinates(int x, int y, MapCoordinates parent) {
        this.x = x;
        this.y = y;
        this.parent = parent;
    }
}
